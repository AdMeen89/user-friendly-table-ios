//
//  ViewController.m
//  CellsExample
//
//  Created by Andrey Zamogilin on 28.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "ViewController.h"
#import "UFTableView.h"
#import "UFTVCellStoryboard.h"
#import "UFTVCell.h"
#import "UFTVCellDefault.h"
#import "UFTVCellAction.h"
#import "UFTVCellSet.h"

@interface ViewController ()<UFTableViewDataSource>

@property (weak, nonatomic) IBOutlet UFTableView *tableView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.dataSourceUF = self;
    [self.tableView addSection:[self sectionWithDefault]];
    [self.tableView addSection:[self sectionWithStoryboard]];
}


- (UFTVSection *)sectionWithDefault
{
    NSMutableArray *array = [NSMutableArray array];
    
    for (int i = 0; i < 10; i++)
    {
         UFTVCellDefault *cell = [UFTVCellDefault cellWithTitle:@"Default table cell" height:40];
        [array addObject:cell];
    }
    
   
    UFTVSection *section = [UFTVSection sectionWithArrayCells:array];
    UFTVCellSet *all = [section all];
    
    UFTVCellAction *action = [UFTVCellAction actionWithConfigureCellBlock:
                              ^id(UITableViewCell *cell, NSIndexPath *indexPath)
    {
        cell.textLabel.text = @"Configure block";
        return cell;
    }];
    
    UFTVCellAction *selectAction = [UFTVCellAction actionWithDidSelectCellBlock:
                                    ^id(UFTVCellDefault *cell, NSIndexPath *indexPath)
    {
        [cell hideWithAnimation:YES];
        return nil;
    }];
//
//    UFTVCellAction *deselectAction = [UFTVCellAction actionWithDidSelectCellBlock:^id(UFTVCellDefault *cell, NSIndexPath *indexPath) {
//        NSLog(@"Cell didDeselect");
//        return nil;
//    }];
    
    all.actionConfigure = action;
    all.actionDidSelect = selectAction;
    
    return section;
}

- (UFTVSection *)sectionWithStoryboard
{
    UFTVCellStoryboard *cell = [UFTVCellStoryboard cellWithId:@"Test" height:20];
    UFTVSection *section = [UFTVSection sectionWithArrayCells:@[cell]];
    
    return section;
}

@end
