//
//  UFTableView.h
//  SportsUniform
//
//  Created by Andrey Zamogilin on 16.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UFTVSection.h"
#import "UFTVCell.h"
#import "UFTVSectionHeader.h"

@protocol UFTableViewDataSource <NSObject>

@optional

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView;
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index;
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath;

@end

@interface UFTableView : UITableView

@property (weak, nonatomic)id<UFTableViewDataSource> dataSourceUF;
@property (nonatomic) NSInteger countOfSection;

- (UFTVSection *)objectAtIndexedSubscript:(NSInteger)idx;
- (void)addSection:(UFTVSection *)sectionInfo;
- (UFTVCell *)cellInfoWithIndexPath:(NSIndexPath *)indexPath;

@end
