//
//  UFTVCellSet.m
//  UFTableView
//
//  Created by Andrey Zamogilin on 01.03.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTVCellSet.h"
#import "UFTVCell.h"

@interface UFTVCellSet ()

@property (strong, nonatomic) NSMutableArray *cellSet;
@property (nonatomic) BOOL hidden;

@end

@implementation UFTVCellSet

#pragma mark - 
#pragma mark - Initialize

-(UFTVCellSet *)initWithArray:(NSArray *)array
{
    self = [super init];
    
    if (self)
    {
        self.cellSet = [NSMutableArray arrayWithArray:array];
    }
    
    return self;
}

+(UFTVCellSet *)setWithArray:(NSArray *)array
{
    return [[UFTVCellSet alloc] initWithArray:array];
}

#pragma mark - 
#pragma mark - Add objects

-(void)addObjectsFromArray:(NSArray *)array
{
    [self.cellSet addObjectsFromArray:array];
}

#pragma mark -
#pragma mark - Setters

- (void)setCanEditRow:(BOOL)canEditRow
{
    _canEditRow = canEditRow;
    
    for (UFTVCell *cell in self.cellSet)
    {
        cell.canEditRow = _canEditRow;
    }
}

- (void)setCanMoveRow:(BOOL)canMoveRow
{
    _canMoveRow = canMoveRow;
    
    for (UFTVCell *cell in self.cellSet)
    {
        cell.canMoveRow = _canMoveRow;
    }
}

-(void)setActionConfigure:(UFTVCellAction *)actionConfigure
{
    _actionConfigure = actionConfigure;
    
    for (UFTVCell *cell in self.cellSet)
    {
        cell.actionConfigure = _actionConfigure;
    }
}

-(void)setActionDidSelect:(UFTVCellAction *)actionDidSelect
{
    _actionDidSelect = actionDidSelect;
    
    for (UFTVCell *cell in self.cellSet)
    {
        cell.actionDidSelect = _actionDidSelect;
    }
}

-(void)setActionDidDeselect:(UFTVCellAction *)actionDidDeselect
{
    _actionDidDeselect = actionDidDeselect;
    
    for (UFTVCell *cell in self.cellSet)
    {
        cell.actionDidDeselect = _actionDidDeselect;
    }
}

-(void)toggleWithAnimation:(BOOL)animation
{
    if (self.hidden)
    {
        [self showWithAnimation:animation];
    }
    else
    {
        [self hideWithAnimation:animation];
    }
}

-(void)hideWithAnimation:(BOOL)animation
{
    self.hidden = YES;
    
    for (UFTVCell *cell in self.cellSet)
    {
        [cell hideWithAnimation:animation];
    }
}

-(void)showWithAnimation:(BOOL)animation
{
    self.hidden = NO;
    
    for (UFTVCell *cell in self.cellSet)
    {
        [cell showWithAnimation:animation];
    }
}



@end
