//
//  UFTVCellSet.h
//  UFTableView
//
//  Created by Andrey Zamogilin on 01.03.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UFTVCell.h"

@class UFTVCellAction;

@interface UFTVCellSet : NSObject < UFTVCellProtocol>

@property (nonatomic) BOOL canEditRow;
@property (nonatomic) BOOL canMoveRow;

@property (strong, nonatomic) UFTVCellAction *actionConfigure;
@property (strong, nonatomic) UFTVCellAction *actionDidSelect;
@property (strong, nonatomic) UFTVCellAction *actionDidDeselect;

@property (nonatomic, readonly) BOOL hidden;

+(UFTVCellSet *)setWithArray:(NSArray *)array;

-(void)addObjectsFromArray:(NSArray *)array;

@end
