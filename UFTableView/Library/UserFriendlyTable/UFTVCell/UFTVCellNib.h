//
//  UFTVCellNib.h
//  UFTableView
//
//  Created by Andrey Zamogilin on 01.03.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTVCell.h"

@interface UFTVCellNib : UFTVCell

@property (copy, nonatomic) NSString *nibName;

+ (UFTVCellNib *)cellWithNibName:(NSString *)nibName height:(CGFloat)height;

@end
