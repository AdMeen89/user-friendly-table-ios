//
//  UFTableViewCellInfo.m
//  SportsUniform
//
//  Created by Andrey Zamogilin on 16.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTVCell.h"
#import "UFTableView.h"
#import "UFTVSection.h"

@interface UFTVCell ()

@property (strong, nonatomic) NSIndexPath *indexPath;
@property (weak, nonatomic) UFTVSection *sectionInfo;
@property (weak, nonatomic) UITableViewCell *cellObject;
@property (nonatomic) BOOL hidden;
@end

@implementation UFTVCell

#pragma mark -
#pragma mark - UFTVCellMakeProtocol

+(UITableViewCell *)cellWithTable:(UFTableView *)tableView sectionInfo:(UFTVSection *)sectionInfo
                        indexPath:(NSIndexPath *)indexPath
{
    return nil;
}

+(void)didSelectWithTable:(UFTableView *)tableView
              sectionInfo:(UFTVSection *)sectionInfo
                indexPath:(NSIndexPath *)indexPath
{
    UFTVCell *cellInfo = (UFTVCell *)sectionInfo[indexPath.item];
    if (cellInfo.actionDidSelect)
    {
        [cellInfo.actionDidSelect performActionWithCell:cellInfo indexPath:indexPath];
    }
}

+(void)didDeselectWithTable:(UFTableView *)tableView
                sectionInfo:(UFTVSection *)sectionInfo
                  indexPath:(NSIndexPath *)indexPath
{
    UFTVCell *cellInfo = (UFTVCell *)sectionInfo[indexPath.item];
    if (cellInfo.actionDidDeselect)
    {
        [cellInfo.actionDidDeselect performActionWithCell:cellInfo indexPath:indexPath];
    }
}

#pragma mark -
#pragma mark - getters

-(UFTVCell *)next
{
    NSInteger nextIndex = self.indexPath.row + 1;
    return self.sectionInfo[nextIndex];
}

-(UFTVCell *)prev
{
    NSInteger nextIndex = self.indexPath.row - 1;
    return self.sectionInfo[nextIndex];
}

#pragma mark -
#pragma mark - setters

- (void)setHeight:(CGFloat)height
{
    [self setHeight:height animated:NO];
}


#pragma mark -
#pragma mark - update

-(void)updateInfoWithIndexPath:(NSIndexPath *)indexPath sectionInfo:(UFTVSection *)sectionInfo
                    cellObject:(UITableViewCell *)cellObject
{
    self.indexPath = indexPath;
    self.sectionInfo = sectionInfo;
    self.cellObject = cellObject;
}

#pragma mark -
#pragma mark - behavior

-(void)hideWithAnimation:(BOOL)animation
{
    self.height = 0;
    UITableViewRowAnimation animationType = animation ? UITableViewRowAnimationAutomatic : UITableViewRowAnimationNone;
    [self.sectionInfo.tableView reloadRowsAtIndexPaths:@[self.indexPath] withRowAnimation:animationType];
    self.hidden = YES;
}

-(void)showWithAnimation:(BOOL)animation
{
    self.height = 0;
    UITableViewRowAnimation animationType = animation ? UITableViewRowAnimationAutomatic : UITableViewRowAnimationNone;
    [self.sectionInfo.tableView reloadRowsAtIndexPaths:@[self.indexPath] withRowAnimation:animationType];
    self.hidden = NO;
}

-(void)toggleWithAnimation:(BOOL)animation
{
    if (self.hidden)
    {
        [self showWithAnimation:animation];
    } else
    {
        [self hideWithAnimation:animation];
    }
}

-(void)setHeight:(CGFloat)height animated:(BOOL)animated
{
    _height = height;
    if (self.cellObject)
    {
        UITableViewRowAnimation animationType = animated ? UITableViewRowAnimationAutomatic : UITableViewRowAnimationNone;
        [self.sectionInfo.tableView reloadRowsAtIndexPaths:@[self.indexPath] withRowAnimation:animationType];
    }
}

@end
