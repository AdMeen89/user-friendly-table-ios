//
//  UFTVCellStoryboard.m
//  UFTableView
//
//  Created by Andrey Zamogilin on 28.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTVCellStoryboard.h"
#import "UFTVSection.h"
#import "UFTableView.h"

@interface UFTVCellStoryboard ()

@property(copy, nonatomic) NSString *cellId;

@end

@implementation UFTVCellStoryboard

#pragma mark -
#pragma mark - Initialize

-(UFTVCellStoryboard *)initWithId:(NSString *)cellId
{
    self = [super init];
    
    if(self)
    {
        self.cellId = cellId;
        self.height = UFTableViewCellHeightZero;
        self.canMoveRow = NO;
        self.canEditRow = NO;
    }
    
    return self;
}

+(UFTVCellStoryboard *)cellWithId:(NSString *)cellId
{
    return [[UFTVCellStoryboard alloc] initWithId: cellId];
}

-(UFTVCellStoryboard *)initWithId:(NSString *)cellId height:(CGFloat)height
{
    self = [super init];
    
    if(self)
    {
        self.cellId = cellId;
        self.height = height;
    }
    
    return self;
}

+(UFTVCellStoryboard *)cellWithId:(NSString *)cellId height:(CGFloat)height
{
    return [[UFTVCellStoryboard alloc] initWithId: cellId height:height];
}


#pragma mark -
#pragma mark - UFTVCellMakeProtocol

+(UITableViewCell *)cellWithTable:(UFTableView *)tableView sectionInfo:(UFTVSection *)sectionInfo
                        indexPath:(NSIndexPath *)indexPath
{
    UFTVCellStoryboard *cellInfo = (UFTVCellStoryboard *)sectionInfo[indexPath.item];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellInfo.cellId forIndexPath:indexPath];
    
    return cell;
}


@end
