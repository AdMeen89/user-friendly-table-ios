//
//  UFTVCellDefault.h
//  UFTableView
//
//  Created by Andrey Zamogilin on 28.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTVCell.h"

@interface UFTVCellDefault : UFTVCell

@property (copy, nonatomic) NSString *title;

#pragma mark - init
+(UFTVCellDefault *)cellWithTitle:(NSString *)title height:(CGFloat)height;

@end
