//
//  UFTVCellStoryboard.h
//  UFTableView
//
//  Created by Andrey Zamogilin on 28.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTVCell.h"

@interface UFTVCellStoryboard : UFTVCell

// Cell identifier
@property (copy, nonatomic, readonly) NSString *cellId;

#pragma mark - init

+(UFTVCellStoryboard *)cellWithId:(NSString *)cellId;
+(UFTVCellStoryboard *)cellWithId:(NSString *)cellId height:(CGFloat)height;

@end
