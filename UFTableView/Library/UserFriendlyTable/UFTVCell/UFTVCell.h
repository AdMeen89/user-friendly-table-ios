//
//  UFTableViewCellInfo.h
//  SportsUniform
//
//  Created by Andrey Zamogilin on 16.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UFTVCellAction.h"


typedef NS_ENUM(NSInteger, UFTableViewCellHeight)
{
    UFTableViewCellHeightZero = -1
};

@class UFTVSection, UFTableView, UFTVCell;

@protocol UFTVCellMakeProtocol <NSObject>

+(UITableViewCell *)cellWithTable:(UFTableView *)tableView
                      sectionInfo:(UFTVSection *)sectionInfo
                        indexPath:(NSIndexPath *)indexPath;

+(void)didSelectWithTable:(UFTableView *)tableView
              sectionInfo:(UFTVSection *)sectionInfo
                indexPath:(NSIndexPath *)indexPath;

+(void)didDeselectWithTable:(UFTableView *)tableView
              sectionInfo:(UFTVSection *)sectionInfo
                indexPath:(NSIndexPath *)indexPath;

@end

@protocol UFTVCellProtocol <NSObject>

@property (nonatomic) BOOL canEditRow;
@property (nonatomic) BOOL canMoveRow;

@property (strong, nonatomic) UFTVCellAction *actionConfigure;
@property (strong, nonatomic) UFTVCellAction *actionDidSelect;
@property (strong, nonatomic) UFTVCellAction *actionDidDeselect;

@property (nonatomic, readonly) BOOL hidden;

- (void)toggleWithAnimation:(BOOL)animation;
- (void)hideWithAnimation:(BOOL)animation;
- (void)showWithAnimation:(BOOL)animation;

@end



@interface UFTVCell : NSObject <UFTVCellMakeProtocol, UFTVCellProtocol>

@property (nonatomic, readonly) NSIndexPath *indexPath;
@property (weak, nonatomic, readonly) UFTVSection *sectionInfo;
@property (weak, nonatomic, readonly) UITableViewCell *cellObject;
@property (nonatomic) CGFloat height;

@property (nonatomic) BOOL canEditRow;
@property (nonatomic) BOOL canMoveRow;

@property (strong, nonatomic) UFTVCellAction *actionConfigure;
@property (strong, nonatomic) UFTVCellAction *actionDidSelect;
@property (strong, nonatomic) UFTVCellAction *actionDidDeselect;

@property (nonatomic, readonly) BOOL hidden;

@property (strong, nonatomic) UFTVCell *next;
@property (strong, nonatomic) UFTVCell *prev;

#pragma mark - update

-(void)updateInfoWithIndexPath:(NSIndexPath *)indexPath sectionInfo:(UFTVSection *)sectionInfo
                    cellObject:(UITableViewCell *)cellObject;

#pragma mark - behavior

-(void)toggleWithAnimation:(BOOL)animation;
-(void)hideWithAnimation:(BOOL)animation;
-(void)showWithAnimation:(BOOL)animation;

-(void)setHeight:(CGFloat)height animated:(BOOL)animated;

@end
