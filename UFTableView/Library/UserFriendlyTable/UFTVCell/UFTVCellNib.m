//
//  UFTVCellNib.m
//  UFTableView
//
//  Created by Andrey Zamogilin on 01.03.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTVCellNib.h"

@implementation UFTVCellNib

#pragma mark -
#pragma mark - Initialize

- (UFTVCellNib *)initWithNibName:(NSString *)nibName height:(CGFloat)height
{
    self = [super init];
    
    if (self)
    {
        self.height = height;
        self.nibName = nibName;
    }
    
    return self;
}

+ (UFTVCellNib *)cellWithNibName:(NSString *)nibName height:(CGFloat)height
{
    return [[UFTVCellNib alloc] initWithNibName:nibName height:height];
}

#pragma mark -
#pragma mark - UFTVCellMakeProtocol

+(UITableViewCell *)cellWithTable:(UFTableView *)tableView sectionInfo:(UFTVSection *)sectionInfo
                        indexPath:(NSIndexPath *)indexPath
{
    return nil;
}

@end
