//
//  UFTVCellDefault.m
//  UFTableView
//
//  Created by Andrey Zamogilin on 28.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTVCellDefault.h"
#import "UFTVSection.h"
#import "UFTableView.h"

@implementation UFTVCellDefault

#pragma mark -
#pragma mark - Initialize

-(UFTVCellDefault *)initWithTitle:(NSString *)title height:(CGFloat)height
{
    self = [super init];
    
    if (self)
    {
        self.title = title;
        self.height = height;
    }
    
    return self;
}

+(UFTVCellDefault *)cellWithTitle:(NSString *)title height:(CGFloat)height
{
    return [[UFTVCellDefault alloc] initWithTitle:title height:height];
}

#pragma mark -
#pragma mark - UFTVCellMakeProtocol

+(UITableViewCell *)cellWithTable:(UFTableView *)tableView sectionInfo:(UFTVSection *)sectionInfo
                        indexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UFTVCellDefault *cellInfo = (UFTVCellDefault *)sectionInfo[indexPath.item];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.textLabel.text = cellInfo.title;
    
    if (cellInfo.actionConfigure)
    {
        cell = [cellInfo.actionConfigure performActionWithCell:cell indexPath:indexPath];
    }

    return cell;
}

@end
