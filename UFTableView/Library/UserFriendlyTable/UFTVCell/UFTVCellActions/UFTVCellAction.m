//
//  UFTVActions.m
//  UFTableView
//
//  Created by Andrey Zamogilin on 28.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTVCellAction.h"

@interface UFTVCellAction ()

@property (nonatomic) SEL configureCellSelector;
@property (strong, nonatomic) UFTVCellActionBlock configureCellBlock;

@property (nonatomic) SEL didSelectCellSelector;
@property (strong, nonatomic) UFTVCellActionBlock didSelectCellBlock;

@property (nonatomic) SEL didDeselectCellSelector;
@property (strong, nonatomic) UFTVCellActionBlock didDeselectCellBlock;

@property (nonatomic) SEL activeSelector;
@property (strong, nonatomic) UFTVCellActionBlock activeBlock;

@property (strong, nonatomic) id target;

@end

@implementation UFTVCellAction

#pragma mark -
#pragma mark - Initialize


- (UFTVCellAction *)initWithConfigureCellSelector:(SEL)selector target:(id)target
{
    self = [super init];
    
    if (self)
    {
        self.configureCellSelector = selector;
    }
    
    return self;
}

- (UFTVCellAction *)initWithDidSelectCellSelector:(SEL)selector target:(id)target
{
    self = [super init];
    
    if (self)
    {
        self.didSelectCellSelector = selector;
    }
    
    return self;
}

- (UFTVCellAction *)initWithDidDeselectCellSelector:(SEL)selector target:(id)target
{
    self = [super init];
    
    if (self)
    {
        self.didDeselectCellSelector = selector;
    }
    
    return self;
}

- (UFTVCellAction *)initWithConfigureCellBlock:(UFTVCellActionBlock)block
{
    self = [super init];
    
    if (self)
    {
        self.configureCellBlock = block;
    }
    
    return self;
}

- (UFTVCellAction *)initWithDidSelectCellBlock:(UFTVCellActionBlock)block
{
    self = [super init];
    
    if (self)
    {
        self.didSelectCellBlock = block;
    }
    
    return self;
}

- (UFTVCellAction *)initWithDidDeselectCellBlock:(UFTVCellActionBlock)block
{
    self = [super init];
    
    if (self)
    {
        self.didDeselectCellBlock = block;
    }
    
    return self;
}

+ (UFTVCellAction *)actionWithConfigureCellSelector:(SEL)selector target:(id)target
{
    return [[UFTVCellAction alloc] initWithConfigureCellSelector:selector target:target];
}

+ (UFTVCellAction *)actionWithDidSelectCellSelector:(SEL)selector target:(id)target
{
     return [[UFTVCellAction alloc] initWithDidSelectCellSelector:selector target:target];
}

+ (UFTVCellAction *)actionWithDidDeselectCellSelector:(SEL)selector target:(id)target
{
     return [[UFTVCellAction alloc] initWithDidDeselectCellSelector:selector target:target];
}

+ (UFTVCellAction *)actionWithConfigureCellBlock:(UFTVCellActionBlock)block
{
    return [[UFTVCellAction alloc] initWithConfigureCellBlock:block];
}

+ (UFTVCellAction *)actionWithDidSelectCellBlock:(UFTVCellActionBlock)block
{
    return [[UFTVCellAction alloc] initWithDidSelectCellBlock:block];
}

+ (UFTVCellAction *)actionWithDidDeselectCellBlock:(UFTVCellActionBlock)block
{
    return [[UFTVCellAction alloc] initWithDidDeselectCellBlock:block];
}

#pragma mark -
#pragma mark - Other

- (id)performActionWithCell:(UITableViewCell *)cell indexPath:(NSIndexPath *)indexPath
{
    if (self.activeBlock != nil)
    {
        return self.activeBlock(cell, indexPath);
    }
    
    if (self.activeSelector && self.target && [self.target respondsToSelector:self.activeSelector])
    {
        IMP imp = [self.target methodForSelector:self.activeSelector];
        id (*func)(UITableViewCell *, NSIndexPath *) = (void *)imp;
        return func(cell, indexPath);
    }
    
    return nil;
}

#pragma mark -
#pragma mark - Setters

- (void)setConfigureCellSelector:(SEL)configureCellSelector
{
    _configureCellSelector = configureCellSelector;
    self.activeSelector = _configureCellSelector;
}

- (void)setDidSelectCellSelector:(SEL)didSelectCellSelector
{
    _didSelectCellSelector = didSelectCellSelector;
    self.activeSelector = _didSelectCellSelector;
}

- (void)setDidDeselectCellSelector:(SEL)didDeselectCellSelector
{
    _didDeselectCellSelector = didDeselectCellSelector;
    self.activeSelector = _didDeselectCellSelector;
}

- (void)setConfigureCellBlock:(UFTVCellActionBlock)configureCellBlock
{
    _configureCellBlock = configureCellBlock;
    self.activeBlock = _configureCellBlock;
}

- (void)setDidSelectCellBlock:(UFTVCellActionBlock)didSelectCellBlock
{
    _didSelectCellBlock = didSelectCellBlock;
    self.activeBlock = _didSelectCellBlock;
}

- (void)setDidDeselectCellBlock:(UFTVCellActionBlock)didDeselectCellBlock
{
    _didDeselectCellBlock = didDeselectCellBlock;
    self.activeBlock = _didDeselectCellBlock;
}

@end
