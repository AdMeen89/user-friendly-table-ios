//
//  UFTVActions.h
//  UFTableView
//
//  Created by Andrey Zamogilin on 28.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef id (^UFTVCellActionBlock)(id cell, NSIndexPath *indexPath);

@interface UFTVCellAction : NSObject

@property (nonatomic, readonly) SEL configureCellSelector;
@property (nonatomic, strong, readonly) UFTVCellActionBlock configureCellBlock;

@property (nonatomic, readonly) SEL didSelectCellSelector;
@property (nonatomic, strong, readonly) UFTVCellActionBlock didSelectCellBlock;

@property (nonatomic, readonly) SEL didDeselectCellSelector;
@property (nonatomic, strong, readonly) UFTVCellActionBlock didDeselectCellBlock;

+ (UFTVCellAction *)actionWithConfigureCellSelector:(SEL)selector target:(id)target;
+ (UFTVCellAction *)actionWithDidSelectCellSelector:(SEL)selector target:(id)target;
+ (UFTVCellAction *)actionWithDidDeselectCellSelector:(SEL)selector target:(id)target;

+ (UFTVCellAction *)actionWithConfigureCellBlock:(UFTVCellActionBlock)block;
+ (UFTVCellAction *)actionWithDidSelectCellBlock:(UFTVCellActionBlock)block;
+ (UFTVCellAction *)actionWithDidDeselectCellBlock:(UFTVCellActionBlock)block;

- (id)performActionWithCell:(id)cell indexPath:(NSIndexPath *)indexPath;

@end
