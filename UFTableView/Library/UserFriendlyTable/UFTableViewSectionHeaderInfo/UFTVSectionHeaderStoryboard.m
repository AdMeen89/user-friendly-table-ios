//
//  UFTVSectionHeaderStoryboard.m
//  UFTableView
//
//  Created by Andrey Zamogilin on 28.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTVSectionHeaderStoryboard.h"
#import "UFTVSection.h"

@interface UFTVSectionHeaderStoryboard ()
@property (copy, nonatomic) NSString *cellId;
@end

@implementation UFTVSectionHeaderStoryboard

#pragma mark -
#pragma mark - Initialize

- (UFTVSectionHeaderStoryboard *)initWithCellId:(NSString *)cellId height:(CGFloat)height
{
    self = [super init];
    
    if (self)
    {
        self.height = height;
        self.cellId = cellId;
    }
    
    return self;
}

+ (UFTVSectionHeaderStoryboard *)headerWithCellId:(NSString *)cellId height:(CGFloat)height
{
    return [[UFTVSectionHeaderStoryboard alloc] initWithCellId:cellId height:height];
}

#pragma mark -
#pragma mark - other

+ (UIView *)viewFromTableView:(UITableView *)tableView withCellId:(NSString *)cellId
{
    UITableViewCell *sectionView = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (!sectionView)
    {
        sectionView = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    
    while (sectionView.contentView.gestureRecognizers.count)
    {
        [sectionView.contentView removeGestureRecognizer:[sectionView.contentView.gestureRecognizers objectAtIndex:0]];
    }
    
    return sectionView;
}


#pragma mark -
#pragma mark - UFTVSectionHeaderMakeProtocol


+ (UIView *)sectionHeaderViewWithTableView:(UFTableView *)tableView
                           sectionInfo:(UFTVSection *)sectionInfo
                         forHeaderType:(UFTVSectionHeaderType)type
{
    
    UFTVSectionHeaderStoryboard *header = (UFTVSectionHeaderStoryboard *)sectionInfo.footer;
    
    if (type == UFTableViewSectionHeaderTypeHeader)
    {
        header = (UFTVSectionHeaderStoryboard *)sectionInfo.header;
    }
    
    if (header && header.cellId)
    {
        return [UFTVSectionHeaderStoryboard viewFromTableView:(UITableView *)tableView withCellId:header.cellId];
    }
    
    return nil;
}



@end
