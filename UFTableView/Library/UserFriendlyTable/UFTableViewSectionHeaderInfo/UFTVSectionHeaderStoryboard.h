//
//  UFTVSectionHeaderStoryboard.h
//  UFTableView
//
//  Created by Andrey Zamogilin on 28.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTVSectionHeader.h"

@interface UFTVSectionHeaderStoryboard : UFTVSectionHeader

// Cell identifier from storyboard.
@property (copy, nonatomic, readonly) NSString *cellId;

// Make header from storyboard cell.
// @param cellId A cell identifier that will be used as a prototype for header.
// @param height Height of cell.
+ (UFTVSectionHeaderStoryboard *)headerWithCellId:(NSString *)cellId height:(CGFloat)height;

@end
