//
//  UFTVSectionHeaderBlock.h
//  UFTableView
//
//  Created by Andrey Zamogilin on 28.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTVSectionHeader.h"

typedef UIView * (^UFTVSectionHeaderInfo_makeViewBlock)();

@interface UFTVSectionHeaderBlock : UFTVSectionHeader

// Block for make header.
@property (strong, nonatomic, readonly) UFTVSectionHeaderInfo_makeViewBlock makeViewBlock;

// Make header from create view block.
// @param cellId A cell identifier that will be used as a prototype for header.
// @param height Height of cell.
+ (UFTVSectionHeaderBlock *)headerWithHeight:(CGFloat)height makeViewBlock:(UFTVSectionHeaderInfo_makeViewBlock)makeViewBlock;

@end
