//
//  UFTVSectionHeaderNib.m
//  UFTableView
//
//  Created by Andrey Zamogilin on 28.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTVSectionHeaderNib.h"
#import "UFTVSection.h"

@implementation UFTVSectionHeaderNib

#pragma mark -
#pragma mark - Initialize

-(UFTVSectionHeaderNib *)initNibName:(NSString *)nibName height:(CGFloat)height
{
    self = [super init];
    if (self)
    {
        self.nibName = nibName;
        self.height = height;
    }
    
    return self;
}

+(UFTVSectionHeaderNib *)headerWithNibName:(NSString *)nibName height:(CGFloat)height
{
    return [[UFTVSectionHeaderNib alloc] initNibName:nibName height:height];
}


#pragma mark -
#pragma mark - UFTVSectionHeaderMakeProtocol


+ (UIView *)sectionHeaderViewWithTableView:(UFTableView *)tableView
                           sectionInfo:(UFTVSection *)sectionInfo
                         forHeaderType:(UFTVSectionHeaderType)type
{
    UFTVSectionHeaderNib *header = (UFTVSectionHeaderNib *)sectionInfo.footer;
    
    if (type == UFTableViewSectionHeaderTypeHeader)
    {
        header = (UFTVSectionHeaderNib *)sectionInfo.header;
    }
    
    return [self viewFromNibWithNibName:header.nibName];
}

#pragma mark -
#pragma mark - Other

+ (UIView *)viewFromNibWithNibName:(NSString *)nibName
{
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
    return [nibs lastObject];
}

@end
