//
//  UFTVSectionHeaderDefault.m
//  UFTableView
//
//  Created by Andrey Zamogilin on 28.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTVSectionHeaderDefault.h"
#import "UFTVSection.h"

@implementation UFTVSectionHeaderDefault

#pragma mark -
#pragma mark - UFTVSectionHeaderMakeProtocol

+ (NSString *)sectionHeaderTitleWithTableView:(UFTableView *)tableView
                        sectionInfo:(UFTVSection *)sectionInfo
                      forHeaderType:(UFTVSectionHeaderType)type
{
    UFTVSectionHeaderDefault *header = (UFTVSectionHeaderDefault *)sectionInfo.footer;
    
    if (type == UFTableViewSectionHeaderTypeHeader)
    {
        header = (UFTVSectionHeaderDefault *)sectionInfo.header;
    }
    
    return header.title;
}

@end
