//
//  UFTVSectionHeaderBlock.m
//  UFTableView
//
//  Created by Andrey Zamogilin on 28.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTVSectionHeaderBlock.h"
#import "UFTVSection.h"

@interface UFTVSectionHeaderBlock ()

@property (strong, nonatomic) UFTVSectionHeaderInfo_makeViewBlock makeViewBlock;

@end

@implementation UFTVSectionHeaderBlock

#pragma mark -
#pragma mark - Initalize

- (UFTVSectionHeaderBlock *)initWithHeight:(CGFloat)height makeViewBlock:(UFTVSectionHeaderInfo_makeViewBlock)makeViewBlock
{
    self = [super init];
    
    if (self)
    {
        self.height = height;
        self.makeViewBlock = makeViewBlock;
    }
    
    return self;
}

+ (UFTVSectionHeaderBlock *)headerWithHeight:(CGFloat)height makeViewBlock:(UFTVSectionHeaderInfo_makeViewBlock)makeViewBlock
{
    return [[UFTVSectionHeaderBlock alloc] initWithHeight:height makeViewBlock:makeViewBlock];
}

#pragma mark -
#pragma mark - UFTVSectionHeaderMakeProtocol


+ (UIView *)sectionHeaderViewWithTableView:(UFTableView *)tableView
                           sectionInfo:(UFTVSection *)sectionInfo
                         forHeaderType:(UFTVSectionHeaderType)type
{
    
    UFTVSectionHeaderBlock *header = (UFTVSectionHeaderBlock *)sectionInfo.footer;
    
    if (type == UFTableViewSectionHeaderTypeHeader)
    {
        header = (UFTVSectionHeaderBlock *)sectionInfo.header;
    }
    
    if (header && header.makeViewBlock)
    {
        return header.makeViewBlock();
    }
    
    return nil;
}

@end
