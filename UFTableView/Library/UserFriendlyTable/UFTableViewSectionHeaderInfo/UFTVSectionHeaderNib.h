//
//  UFTVSectionHeaderNib.h
//  UFTableView
//
//  Created by Andrey Zamogilin on 28.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTVSectionHeader.h"

@interface UFTVSectionHeaderNib : UFTVSectionHeader

@property (copy, nonatomic) NSString *nibName;

+(UFTVSectionHeaderNib *)headerWithNibName:(NSString *)nibName height:(CGFloat)height;

@end
