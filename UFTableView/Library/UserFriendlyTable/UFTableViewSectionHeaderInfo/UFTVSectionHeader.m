//
//  UFTableViewSectionHeader.m
//  SportsUniform
//
//  Created by Andrey Zamogilin on 16.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTVSectionHeader.h"

@implementation UFTVSectionHeader

#pragma mark -
#pragma mark - UFTVSectionHeaderMakeProtocol

+ (UIView *)sectionHeaderViewWithTableView:(UFTableView *)tableView
                           sectionInfo:(UFTVSection *)sectionInfo
                         forHeaderType:(UFTVSectionHeaderType)type
{
    return nil;
}

+ (NSString *)sectionHeaderTitleWithTableView:(UFTableView *)tableView
                        sectionInfo:(UFTVSection *)sectionInfo
                      forHeaderType:(UFTVSectionHeaderType)type
{
    return nil;
}



@end
