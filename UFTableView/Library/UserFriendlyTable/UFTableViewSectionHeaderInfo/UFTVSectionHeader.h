//
//  UFTableViewSectionHeader.h
//  SportsUniform
//
//  Created by Andrey Zamogilin on 16.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UFTVSection, UFTableView;

typedef NS_ENUM(NSInteger, UFTVSectionHeaderType)
{
    UFTableViewSectionHeaderTypeFooter,
    UFTableViewSectionHeaderTypeHeader,
};


@protocol UFTVSectionHeaderMakeProtocol <NSObject>

+ (UIView *)sectionHeaderViewWithTableView:(UFTableView *)tableView
                           sectionInfo:(UFTVSection *)sectionInfo
                         forHeaderType:(UFTVSectionHeaderType)type;

+ (NSString *)sectionHeaderTitleWithTableView:(UFTableView *)tableView
                        sectionInfo:(UFTVSection *)sectionInfo
                      forHeaderType:(UFTVSectionHeaderType)type;

@end

@interface UFTVSectionHeader : NSObject <UFTVSectionHeaderMakeProtocol>

// Height of header.
@property (nonatomic) CGFloat height;

@end


