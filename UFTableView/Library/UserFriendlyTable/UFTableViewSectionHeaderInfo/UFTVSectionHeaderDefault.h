//
//  UFTVSectionHeaderDefault.h
//  UFTableView
//
//  Created by Andrey Zamogilin on 28.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTVSectionHeader.h"

@interface UFTVSectionHeaderDefault : UFTVSectionHeader

// Title of header.
@property (copy, nonatomic) NSString *title;

@end
