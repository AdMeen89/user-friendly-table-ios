//
//  UFTableViewSectionInfo.m
//  SportsUniform
//
//  Created by Andrey Zamogilin on 16.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTVSection.h"
#import "UFTVCellSet.h"
#import "UFTableView.h"

@interface UFTVSection ()

@property(strong, nonatomic)NSMutableArray *cellInfoSet;

@end

@implementation UFTVSection

#pragma mark -
#pragma mark - init

- (UFTVSection *)initWithArrayCells:(NSArray *)cells
{
    self = [super init];
    
    if (self)
    {
        self.cellInfoSet = [NSMutableArray arrayWithArray:cells];
    }
    
    return self;
}

+ (UFTVSection *)sectionWithArrayCells:(NSArray *)cells
{
    return [[UFTVSection alloc] initWithArrayCells:cells];
}

#pragma mark -
#pragma mark - Added cells

- (void)addCells:(NSArray *)cells
{
    for (UFTVCell *cell in cells)
    {
        [self addCell:cell];
    }
}

- (void)addCell:(UFTVCell *)cell
{
    [self.cellInfoSet addObject:cell];
}

#pragma mark -
#pragma mark - other

- (UFTVCell *)lastCell
{
    return (UFTVCell *)[self.cellInfoSet lastObject];
}

- (UFTVCell *)firstCell
{
    return (UFTVCell *)[self.cellInfoSet firstObject];
}

#pragma mark -
#pragma mark - sets

-(UFTVCellSet *)all
{
    return [UFTVCellSet setWithArray:self.cellInfoSet];
}

-(UFTVCellSet *)cellsWithRange:(NSRange)range
{
    NSArray *subarray = [self.cellInfoSet subarrayWithRange:range];
    return [UFTVCellSet setWithArray:subarray];
}

-(UFTVCellSet *)cellsToIndex:(NSInteger)idx
{
    return [self cellsWithRange:NSMakeRange(0, (NSUInteger)idx)];
}

-(UFTVCellSet *)cellsOfIndex:(NSInteger)idx
{
    return [self cellsWithRange:NSMakeRange((NSUInteger)idx, (self.cellInfoSet.count - (NSUInteger)idx))];
}

#pragma mark -
#pragma mark - getters

- (NSUInteger)countOfCell
{
    return [self.cellInfoSet count];
}

#pragma mark -
#pragma mark - subscript

- (UFTVCell *)objectAtIndexedSubscript:(NSInteger)idx
{
    if (idx < self.cellInfoSet.count && idx >= 0)
    {
        return self.cellInfoSet[(NSUInteger)idx];
    }
    
    return nil;
}

#pragma mark -
#pragma mark - info

-(UFTVSection *)next
{
    NSInteger nextIndex = self.sectionIndex + 1;
    return self.tableView[nextIndex];
}

-(UFTVSection *)prev
{
    NSInteger nextIndex = self.sectionIndex - 1;
    return self.tableView[nextIndex];
}


@end
