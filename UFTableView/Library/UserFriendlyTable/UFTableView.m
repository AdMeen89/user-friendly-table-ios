//
//  UFTableView.m
//  SportsUniform
//
//  Created by Andrey Zamogilin on 16.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "UFTableView.h"

@interface UFTableView ()<UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) NSMutableArray *sections;
@end

@implementation UFTableView

-(UFTableView *)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self)
    {
        [self setup];
    }
    
    return self;
}


-(UFTableView *)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setup];
    }
    
    return self;
}


-(UFTableView *)initWithFrame:(CGRect)rect style:(UITableViewStyle)style
{
    self = [super initWithFrame:rect style:style];
    if (self)
    {
        [self setup];
    }
    
    return self;
}

-(void)setup
{
    self.delegate = self;
    self.dataSource = self;
    
    if(!self.sections)
    {
        self.sections = [NSMutableArray array];
    }
}

#pragma mark - add section

-(void)addSection:(UFTVSection *)sectionInfo
{
    if (![self.sections containsObject:sectionInfo])
    {
        [self.sections addObject:sectionInfo];
        ((UFTVSection *)[self.sections lastObject]).tableView = self;
        ((UFTVSection *)[self.sections lastObject]).sectionIndex = (NSInteger)self.sections.count - 1;
    }
}

#pragma mark - getters

- (UFTVCell *)cellInfoWithIndexPath:(NSIndexPath *)indexPath
{
    return self[indexPath.section][indexPath.row];
}

- (NSInteger)countOfSection
{
    return (NSInteger)self.sections.count;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return (NSInteger)[[self sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    UFTVSection *sectionInfo = self.sections[(NSUInteger)section];
    return (NSInteger)sectionInfo.countOfCell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UFTVSection *sectionInfo = self.sections[(NSUInteger)indexPath.section];
    UFTVCell *cellInfo = [self cellInfoWithIndexPath:indexPath];
    
    UITableViewCell *cell = [[cellInfo class] cellWithTable:self sectionInfo:sectionInfo indexPath:indexPath];
    
    [cellInfo updateInfoWithIndexPath:indexPath sectionInfo:sectionInfo cellObject:cell];
    
    return cell;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    UFTVSection *sectionInfo = self[section];
    
    if (sectionInfo.header)
    {
        return [[sectionInfo.header class] sectionHeaderTitleWithTableView:self sectionInfo:sectionInfo
                                                             forHeaderType:UFTableViewSectionHeaderTypeHeader];
    }
    
    return nil;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    UFTVSection *sectionInfo = self[section];
    
    if (sectionInfo.footer)
    {
        return [[sectionInfo.header class] sectionHeaderTitleWithTableView:self sectionInfo:sectionInfo
                                                             forHeaderType:UFTableViewSectionHeaderTypeFooter];
    }
    
    return nil;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    UFTVCell *cellInfo = [self cellInfoWithIndexPath:indexPath];
    return cellInfo.canEditRow;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    UFTVCell *cellInfo = [self cellInfoWithIndexPath:indexPath];
    return cellInfo.canMoveRow;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (self.dataSourceUF && [self.dataSourceUF respondsToSelector:@selector(sectionIndexTitlesForTableView:)])
    {
        return [self.dataSourceUF sectionIndexTitlesForTableView:tableView];
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (self.dataSourceUF && [self.dataSourceUF respondsToSelector:@selector(tableView:sectionForSectionIndexTitle:atIndex:)])
    {
        return [self.dataSourceUF tableView:tableView sectionForSectionIndexTitle:title atIndex:index];
    }
    
    return 0;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.dataSourceUF && [self.dataSourceUF respondsToSelector:@selector(tableView:commitEditingStyle:forRowAtIndexPath:)])
    {
        [self.dataSourceUF tableView:tableView commitEditingStyle:editingStyle forRowAtIndexPath:indexPath];
    }
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    if (self.dataSourceUF && [self.dataSourceUF respondsToSelector:@selector(tableView:moveRowAtIndexPath:toIndexPath:)])
    {
        [self.dataSourceUF tableView:tableView moveRowAtIndexPath:sourceIndexPath toIndexPath:destinationIndexPath];
    }
}

#pragma mark -
#pragma mark - UITableViewDelegate

#pragma mark - Variable height support

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UFTVCell *cellInfo = [self cellInfoWithIndexPath:indexPath];
    
    if (cellInfo.height >= 0)
    {
        return cellInfo.height;
    }
    
    return cellInfo.sectionInfo.tableView.rowHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    UFTVSection *sectionInfo = self[section];
    
    if (sectionInfo.header && sectionInfo.header.height != UFTableViewCellHeightZero)
    {
        return sectionInfo.header.height;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    UFTVSection *sectionInfo = self[section];
    
    if (sectionInfo.footer && sectionInfo.footer.height != UFTableViewCellHeightZero)
    {
        return sectionInfo.footer.height;
    }
    
    return 0;
}

#pragma mark - Section header & footer views

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UFTVSection *sectionInfo = self[section];
    
    if (sectionInfo.header) {
        return [[sectionInfo.header class] sectionHeaderViewWithTableView:self sectionInfo:sectionInfo
                                                            forHeaderType:UFTableViewSectionHeaderTypeHeader];
    }

    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UFTVSection *sectionInfo = self[section];
    
    if (sectionInfo.footer) {
        return [[sectionInfo.header class] sectionHeaderViewWithTableView:self sectionInfo:sectionInfo
                                                            forHeaderType:UFTableViewSectionHeaderTypeFooter];
    }
    
    return nil;
}

#pragma mark - Called after the user changes the selection.

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UFTVSection *sectionInfo = self[indexPath.section];
    UFTVCell *cellInfo = [self cellInfoWithIndexPath:indexPath];
    [[cellInfo class] didSelectWithTable:self sectionInfo:sectionInfo indexPath:indexPath];
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UFTVSection *sectionInfo = self[indexPath.section];
    UFTVCell *cellInfo = [self cellInfoWithIndexPath:indexPath];
    [[cellInfo class] didDeselectWithTable:self sectionInfo:sectionInfo indexPath:indexPath];
}

#pragma mark - subscript

- (UFTVSection *)objectAtIndexedSubscript:(NSInteger)idx
{
    if (idx < (NSInteger)self.sections.count && idx >= 0)
    {
        return self.sections[(NSUInteger)idx];
    }
    
    return nil;
}
@end
