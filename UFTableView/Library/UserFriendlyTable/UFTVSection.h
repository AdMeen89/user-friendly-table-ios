//
//  UFTableViewSectionInfo.h
//  SportsUniform
//
//  Created by Andrey Zamogilin on 16.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UFTVCell, UFTVSectionHeader, UFTVCellSet, UFTableView;

@interface UFTVSection : NSObject

// TableView
@property (weak, nonatomic) UFTableView *tableView;

// Count of cells in section.
@property (nonatomic, readonly) NSUInteger countOfCell;

// Header for section.
// Object of type UFTableViewSectionHeaderInfo.
@property (strong, nonatomic) UFTVSectionHeader *header;

// Footer for section.
// Object of type UFTableViewSectionHeaderInfo.
@property (strong, nonatomic) UFTVSectionHeader *footer;

@property (nonatomic) NSInteger sectionIndex;
@property (strong, nonatomic) UFTVSection *next;
@property (strong, nonatomic) UFTVSection *prev;



#pragma mark - init
+(UFTVSection *)sectionWithArrayCells:(NSArray *)cells;

#pragma mark - add cell
-(void)addCells:(NSArray *)cells;
-(void)addCell:(UFTVCell *)cell;

#pragma mark - get cell

-(UFTVCell *)lastCell;
-(UFTVCell *)firstCell;
-(UFTVCell *)objectAtIndexedSubscript:(NSInteger)idx;

#pragma mark - cell set

-(UFTVCellSet *)all;
-(UFTVCellSet *)cellsWithRange:(NSRange)range;
-(UFTVCellSet *)cellsToIndex:(NSInteger)idx;
-(UFTVCellSet *)cellsOfIndex:(NSInteger)idx;


@end
