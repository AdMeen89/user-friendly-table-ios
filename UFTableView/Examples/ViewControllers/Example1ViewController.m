//
//  Example1ViewController.m
//  UFTableView
//
//  Created by Andrey Zamogilin on 28.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "Example1ViewController.h"
#import "UFTableView.h"
#import "UFTVSectionHeaderStoryboard.h"

static NSString * const kCellId     = @"cell";
static NSString * const kFooterId   = @"footer";
static NSString * const kHeaderId   = @"header";

@interface Example1ViewController ()<UFTableViewDataSource>
@property (weak, nonatomic) IBOutlet UFTableView *tableView;
@end

@implementation Example1ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.tableView.dataSourceUF = self;
    
    UFTVCellInfo *cell = [UFTVCellInfo cellInfoWithId:kCellId height:40];
    UFTVSectionHeaderStoryboard *header = [UFTVSectionHeaderStoryboard headerWithCellId:kHeaderId height:100 ];
    UFTVSectionHeaderStoryboard *footer = [UFTVSectionHeaderStoryboard headerWithCellId:kFooterId height:20];
    
    UFTVSectionInfo *section = [[UFTVSectionInfo alloc] initWithCells:cell, nil];
    section.header = header;
    section.footer = footer;
    
    [self.tableView addSection:section];
}

@end
