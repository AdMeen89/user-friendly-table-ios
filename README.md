[![Build Status](https://travis-ci.org/AdMeen89/User-friendly-table-iOS.svg?branch=master)](https://travis-ci.org/AdMeen89/User-friendly-table-iOS)

#Table of contents
[TOC]

# Why the library?

In my opinion, the standard tables `UITableView` is very bad.
Only in order to display the table with 2-3 cells you need to write a bunch of the same code.


```
#!objective-c

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

...
```

To get the cell with indexPath you need to write more code! 
For every action we write a lot of one the same code. 

And this long and unreadable structure, like as `if-else-if-...`? I could not look at them anymore.
If you like the standard implementation of the tables in the cocoa, then this library is definitely not for you.

# Install

Install via [CocoaPods](http://cocoapods.org/) 

```
#!bash

pod "UserFriendlyTable", '~> 0.0.1'

```

# Description

`UFTableView` is a full heir `UITableView`, which means that you can use all the features of `UITableView`.

# Using

## Create table object

To create a table object, you can use any of the standard methods, such as: add a table to the storyboard or nib, create programmatically.

## Set delegate


```
#!objective-c

self.tableView.dataSourceUF = self;
```


## Describe table


```
#!objective-c

 UFTVCellStoryboard *cell1 = [UFTVCellStoryboard cellWithId:@"cell1" height:20];
 UFTVCellStoryboard *cell2 = [UFTVCellStoryboard cellWithId:@"cell2" height:30];
 UFTVSectionInfo *section = [UFTVSectionInfo sectionWithArrayCells:@[cell1, cell2]];

 [self.tableView addSection: section];
```

That's all we need to write! You have a table with one section and two cells, it is very simple, is not it?

But this is just display it, you could argue. Let's try to add behavior to the cells.


```
#!objective-c

 UFTVCellStoryboard *cell1 = [UFTVCellStoryboard cellWithId:@"cell1" height:20];
 UFTVCellStoryboard *cell2 = [UFTVCellStoryboard cellWithId:@"cell2" height:30];

 UFTVCellAction *action = [UFTVCellAction actionWithConfigureCellBlock:
                              ^id(UITableViewCell *cell, NSIndexPath *indexPath)
  {
        cell.textLabel.text = @"Configure block";
        return cell;
   }]

 cell1.actionConfigure = action;
 UFTVSectionInfo *section = [UFTVSectionInfo sectionWithArrayCells:@[cell1, cell2]];

 [self.tableView addSection: section];
```
What we just did? Adding a few lines we are able to customize the table cell. Not bad, is not it?

But what if we have a lot of cells, and we want to change the color of the cell background. Do we have to assign actions for each cell?Of course not! We can use a set of cells and apply actions to them.

Let's try to add behavior for few cells.

```
#!objective-c

 UFTVCellStoryboard *cell1 = [UFTVCellStoryboard cellWithId:@"cell1" height:20];
 ...
 UFTVCellStoryboard *cell100 = [UFTVCellStoryboard cellWithId:@"cell100" height:30];

 UFTVCellAction *action = [UFTVCellAction actionWithConfigureCellBlock:
                              ^id(UITableViewCell *cell, NSIndexPath *indexPath)
  {
        cell.contentView.backgroundColor = [UIColor redColor];
        cell.backgroundColor = [UIColor redColor];
        return cell;
   }]

 UFTVCellSet *all = [section all];
 all.actionConfigure = action;

 UFTVSectionInfo *section = [UFTVSectionInfo sectionWithArrayCells:@[cell1, cell2]];

 [self.tableView addSection: section];
```
You still do not like it? Ok, let's add processing clicking on the cell with the help of actions.

```
#!objective-c

 UFTVCellStoryboard *cell1 = [UFTVCellStoryboard cellWithId:@"cell1" height:20];
 ...
 UFTVCellStoryboard *cell100 = [UFTVCellStoryboard cellWithId:@"cell100" height:30];

 UFTVCellAction *action = [UFTVCellAction actionWithConfigureCellBlock:
                              ^id(UITableViewCell *cell, NSIndexPath *indexPath)
  {
        cell.contentView.backgroundColor = [UIColor redColor];
        cell.backgroundColor = [UIColor redColor];
        return cell;
   }];

   UFTVCellAction *selectAction = [UFTVCellAction actionWithDidSelectCellBlock:
                                    ^id(UFTVCellDefault *cell, NSIndexPath *indexPath)
   {
        NSLog(@"Cell with indexPath = %@ didSelect", cell.indexPath);
        return nil;
    }];

 UFTVCellSet *all = [section all];

 all.actionConfigure = action;
 all.actionDidSelect = selectAction;

 UFTVSectionInfo *section = [UFTVSectionInfo sectionWithArrayCells:@[cell1, cell2]];

 [self.tableView addSection: section];
```
Yes I Do! You read it's right, we can get the properties of the cells in the cell object, it seems to me, it was obvious. Why this is not in the standard implementation?