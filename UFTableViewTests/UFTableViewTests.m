//
//  UFTableViewTests.m
//  UFTableViewTests
//
//  Created by Andrey Zamogilin on 01.03.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "UFTableView.h"

@interface UFTableViewTests : XCTestCase
@property (strong, nonatomic) UFTableView *tableView;
@end

@implementation UFTableViewTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    if (!self.tableView)
    {
        self.tableView = [[UFTableView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    }
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    self.tableView = nil;
}


- (void)testTableViewSectionAdds
{
    NSInteger sectionCount = arc4random()%10;
    
    for (NSInteger i = 0; i < sectionCount; i++)
    {
        UFTVSection *section = [UFTVSection sectionWithArrayCells:@[]];
        [self.tableView addSection:section];
    }

    NSAssert(sectionCount == self.tableView.countOfSection, @"sectionCount/addSection in table view does not work properly");
}

- (void)testTableViewSubscript
{
    NSInteger sectionCount = arc4random()%10;
    NSMutableArray *sections = [NSMutableArray array];
    
    for (NSInteger i = 0; i < sectionCount; i++)
    {
        UFTVSection *section = [UFTVSection sectionWithArrayCells:@[]];
        [sections addObject:section];
        [self.tableView addSection:section];
    }
    
    for (NSInteger i = 0; i < sectionCount; i++)
    {
        NSAssert([self.tableView[i] isEqual:sections[i]], @"Subscript getters on tableView  does not work properly.");
    }
}

- (void)testTableViewGetCellAtIndexPathNil
{
    UFTVCell *cell = [[UFTVCell alloc] init];
    UFTVSection *section1 = [UFTVSection sectionWithArrayCells:@[cell]];
    UFTVSection *section2 = [UFTVSection sectionWithArrayCells:@[]];
    [self.tableView addSection:section1];
    [self.tableView addSection:section2];
    
    NSIndexPath *indexPathNil = [NSIndexPath indexPathForItem:2 inSection:2];
    
    NSAssert([self.tableView cellInfoWithIndexPath:indexPathNil] == nil, @"method cellInfoWithIndexPath on tableView finds the object where it should not be.");
}

- (void)testTableViewGetCellAtIndexPathNotNil
{
    UFTVCell *cell = [[UFTVCell alloc] init];
    UFTVSection *section1 = [UFTVSection sectionWithArrayCells:@[cell]];
    UFTVSection *section2 = [UFTVSection sectionWithArrayCells:@[]];
    [self.tableView addSection:section1];
    [self.tableView addSection:section2];
    
    NSIndexPath *indexPathNotNil = [NSIndexPath indexPathForItem:0 inSection:0];
    
    NSAssert([self.tableView cellInfoWithIndexPath:indexPathNotNil] != nil, @"method cellInfoWithIndexPath on tableView finds the object where it should not be.");
    
}



@end
