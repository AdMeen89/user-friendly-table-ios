//
//  UFTVSectionTests.m
//  UFTableView
//
//  Created by Zamogilin Andrey on 04.03.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UFTableView.h"

static const NSInteger kCountOfCells = 10;

@interface UFTVSectionTests : XCTestCase
@property (strong, nonatomic) UFTableView *tableView;
@property (strong, nonatomic) UFTVSection *section;
@end

@implementation UFTVSectionTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    if (!self.tableView)
    {
        self.tableView = [[UFTableView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    }
    
    if (!self.section)
    {
        NSMutableArray *cells = [NSMutableArray array];
        for (NSInteger i = 0;  i < kCountOfCells; i++)
        {
            UFTVCell *cell = [[UFTVCell alloc] init];
            [cells addObject:cell];
        }
        self.section = [UFTVSection sectionWithArrayCells:cells];
        [self.tableView addSection:self.section];
    }
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    self.tableView = nil;
    self.section = nil;
}


- (void)testSectionCountCells
{
    NSAssert(kCountOfCells == self.section.countOfCell, @"CellCount method on section does not work properly.");
}

- (void)testSectionSectionIndex
{
    UFTVSection *section = [UFTVSection sectionWithArrayCells:@[]];
    [self.tableView addSection:section];
    
    NSAssert(section.sectionIndex == 1, @"sectionIndex on section does not work properly.");
}

- (void)testSectionNext
{
    UFTVSection *section = [UFTVSection sectionWithArrayCells:@[]];
    [self.tableView addSection:section];
    
    NSAssert(!section.next && [section isEqual:self.section.next], @"next on section does not work properly.");
}

- (void)testSectionPrev
{
    UFTVSection *section = [UFTVSection sectionWithArrayCells:@[]];
    [self.tableView addSection:section];
    
    NSAssert(!self.section.prev && [self.section isEqual:section.prev], @"prev on section does not work properly.");
}


- (void)testSectionAddCellCount
{
    NSInteger count = self.section.countOfCell;
    NSInteger countOfNewCell = arc4random()%10;
    
    for (NSInteger i = 0; i < countOfNewCell; i++)
    {
        UFTVCell *cell = [[UFTVCell alloc] init];
        [self.section addCell:cell];
    }
    
    NSAssert(count+countOfNewCell == self.section.countOfCell, @"Method addCell on section does not work properly.");
}

- (void)testSectionAddCellConsistency
{
    NSInteger count = self.section.countOfCell;
    NSInteger countOfNewCell = arc4random()%10;
    
    NSMutableArray *cells = [NSMutableArray array];
    
    for (NSInteger i = 0; i < countOfNewCell; i++)
    {
        UFTVCell *cell = [[UFTVCell alloc] init];
        [cells addObject:cell];
        [self.section addCell:cell];
    }
    
    for (NSInteger i = count; i < count+countOfNewCell; i++)
    {
        UFTVCell *cell1 = cells[i-count];
        UFTVCell *cell2 = self.section[i];
        NSAssert([cell1 isEqual:cell2], @"Method addCell on section does not work properly.");
    }
}


@end