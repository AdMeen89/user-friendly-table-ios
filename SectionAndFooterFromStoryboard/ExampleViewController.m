//
//  ExampleViewController.m
//  UFTableView
//
//  Created by Andrey Zamogilin on 28.02.15.
//  Copyright (c) 2015 Andrey Zamogilin. All rights reserved.
//

#import "ExampleViewController.h"
#import "UFTableView.h"

#import "UFTVSectionHeaderStoryboard.h"
#import "UFTVSectionHeaderBlock.h"
#import "UFTVSectionHeaderDefault.h"
#import "UFTVSectionHeaderNib.h"

#import "UFTVCellStoryboard.h"

static NSString * const kCellId     = @"cell";
static NSString * const kFooterId   = @"footer";
static NSString * const kHeaderId   = @"header";

@interface ExampleViewController ()<UFTableViewDataSource>

@property (weak, nonatomic) IBOutlet UFTableView *tableView;

@end

@implementation ExampleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSourceUF = self;
    
    [self.tableView addSection:[self sectionFromStoryboard]];
    [self.tableView addSection:[self sectionFromBlock]];
    [self.tableView addSection:[self sectionWithDefault]];
    [self.tableView addSection:[self sectionWithNib]];
    
}

- (UFTVSection *)sectionFromStoryboard
{
    UFTVCellStoryboard *cell = [UFTVCellStoryboard cellWithId:kCellId height:40];
    UFTVSectionHeaderStoryboard *header = [UFTVSectionHeaderStoryboard headerWithCellId:kHeaderId height:30 ];
    UFTVSectionHeaderStoryboard *footer = [UFTVSectionHeaderStoryboard headerWithCellId:kFooterId height:20];
    
    UFTVSection *section = [UFTVSection sectionWithArrayCells:@[cell]];
    section.header = header;
    section.footer = footer;
    
    return section;
}

- (UFTVSection *)sectionFromBlock
{
    UFTVCellStoryboard *cell = [UFTVCellStoryboard cellWithId:kCellId height:40];
    
    UFTVSectionHeaderBlock *header = [UFTVSectionHeaderBlock headerWithHeight:30 makeViewBlock:^UIView *{
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor greenColor];
        return view;
    }];
    
    UFTVSectionHeaderBlock *footer = [UFTVSectionHeaderBlock headerWithHeight:20 makeViewBlock:^UIView *{
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor orangeColor];
        return view;
    }];

    
    UFTVSection *section = [UFTVSection sectionWithArrayCells:@[cell]];
    section.header = header;
    section.footer = footer;
    
    return section;
}


- (UFTVSection *)sectionWithDefault
{
    UFTVCellStoryboard *cell = [UFTVCellStoryboard cellWithId:kCellId height:40];
    
    UFTVSectionHeaderDefault *header =  [UFTVSectionHeaderDefault new];
    header.title = @"Header default";
    header.height = 30;
    
    UFTVSectionHeaderDefault *footer = [UFTVSectionHeaderDefault new];
    footer.title = @"Footer default";
    footer.height = 20;
    
    
    UFTVSection *section = [UFTVSection sectionWithArrayCells:@[cell]];
    section.header = header;
    section.footer = footer;
    
    return section;
}

- (UFTVSection *)sectionWithNib
{
    UFTVCellStoryboard *cell = [UFTVCellStoryboard cellWithId:kCellId height:40];
    
    UFTVSectionHeaderNib *header = [UFTVSectionHeaderNib headerWithNibName:@"NibHeader" height:20];
    UFTVSectionHeaderNib *footer = [UFTVSectionHeaderNib headerWithNibName:@"NibHeader" height:30];
    
    UFTVSection *section = [UFTVSection sectionWithArrayCells:@[cell]];
    section.header = header;
    section.footer = footer;
    
    return section;
}

@end
